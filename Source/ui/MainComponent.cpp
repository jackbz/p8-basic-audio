/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) :   audio (a)
{
    setSize (500, 400);
    addAndMakeVisible(startAndStopButton);
    startAndStopButton.setButtonText("Start and Stop");
    startAndStopButton.addListener(this);
    
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    startAndStopButton.setBounds(getLocalBounds().reduced(100));
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    
}

void MainComponent::buttonClicked (Button* button)
{
    if (helloMate.isItRunning() == true)
    {
        helloMate.stop();
    }
    else if (helloMate.isItRunning() == false)
    {
        helloMate.start();
    }
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("PREFERENCES",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

