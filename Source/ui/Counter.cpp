//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Jack on 13/11/2017.
//
//

#include "Counter.hpp"




MyCounter::MyCounter() : Thread ("CounterThread"), listener (nullptr)
{
    
}

MyCounter::~MyCounter()
{
    stopThread(500);
}


void MyCounter::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (counterValue++);
        Time::waitForMillisecondCounter (time + 200);
    }
}

void MyCounter::start()
{
    startThread();
}

void MyCounter::stop()
{
    stopThread(500);
}

bool MyCounter::isItRunning()
{
    if (isThreadRunning() == true)
            {
                return true;
            }
            else if (isThreadRunning() == false)
            {
                return false;
            }
    return false;
}

void MyCounter::setListener (Listener* newListener)
{
    listener = newListener;
}
