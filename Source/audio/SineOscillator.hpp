//
//  SineOscillator.hpp
//  JuceBasicAudio
//
//  Created by Jack on 12/11/2017.
//
//

#ifndef SineOscillator_hpp
#define SineOscillator_hpp

#include <stdio.h>

class SineOscillator
{
public:
    SineOscillator();
    
    ~SineOscillator();
    
    float getSample();
    
    void setFrequency(float freq);
    
    void setAmplitude(float amp);
    
private:
    float frequency;
    float phasePosition;
    float sampleRate;
    float amplitude;
    float scaledPhase;
    float phaseIncrement;
};

    
    
    
#endif /* SineOscillator_hpp */
