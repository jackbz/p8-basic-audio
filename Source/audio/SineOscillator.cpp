//
//  SineOscillator.cpp
//  JuceBasicAudio
//
//  Created by Jack on 12/11/2017.
//
//

#include "SineOscillator.hpp"
#include <cmath>

SineOscillator::SineOscillator()
{
    frequency = 440.f;
    sampleRate = 44100;
    amplitude = 0.f;
    phasePosition = 0.f;
}

SineOscillator::~SineOscillator()
{
    
}

float SineOscillator::getSample()
{
    
    phasePosition += phaseIncrement;
    
    if (phasePosition > 2 * M_PI)
    {
        phasePosition = phasePosition - 2 * M_PI;
    }
    scaledPhase = sin(phasePosition)*amplitude;
    return scaledPhase;
}

void SineOscillator::setFrequency(float freq)
{
    frequency = freq;
    phaseIncrement = (2 * M_PI * frequency )/sampleRate;
}

void SineOscillator::setAmplitude(float amp)
{
    amplitude = amp/127.f;
}
